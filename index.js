const express = require("express");

const app = express();
//app is our server

const port = 4000;
//port is a variable to contain the port we when we want to designate

app.use(express.json());

let users = [

	{
		email: "mighty12@gmail.com",
		username: "mightyMouse12",
		password: "notrealtedtomickey",
		isAdmin: false
	},
	{
		email: "minnieMouse@gmail.com",
		username: "minniexmickey",
		password: "minniesincethestart",
		isAdmin: false
	},
	{
		email: "mickeyTheMouse@gmail.com",
		username: "MickeyKing",
		password: "thefacethatrunstheplace",
		isAdmin: true
	}
];

let items = [];

let loggedUser = 

//express has methods to use as routes corresponding to each HTTP method
//get(<endpoint>,<functionToHandle request and responses>)

app.get('/hello',(req,res)=>{
//express.json() allows us to handle the requests body and automatically pare the incoming JSON to a JS object we can access and manage.

	//Once that route is accessed, we cans send a response with the use of res.send()
	//res.send() actually combines writeHead() and end() already
	res.send("Hello from Batch 123");
});

app.post('/',(req,res)=>{

	//req.body will contain the body of a request
	console.log(req.body);

	res.send("Hello, I like basketball")
});

//register
app.post('/users', (req,res)=>{

		//since 2 applications are communicating with one another, being our client and our apim it is a good practice to always console log the incoming data first.
		console.log(req.body);

		//simulate creation of new user document
		let newUser = {

			email: req.body.email,
			username: req.body.username,
			password: req.body.password,
			isAdmin: req.body.isAdmin
		};

		users.push(newUser);
		console.log(users);

		res.send("Registered Scuccessfully.");

})

//login
app.post('/users/login',(req,res)=>{

		console.log(req.body);

		let foundUser = users.find((user)=>{

			return user.username === req.body.username && user.password === req.body.password;

		});

		if(foundUser !== undefined){



		//Temporarily log our user in. Allow us to refer the details of a logged in user.
		loggedUser = foundUser;
		console.log(loggedUser);

		res.send('Thank you for logging in.');
	} else {

		res.send('Login failed. Wrong Credentials');
	}
})


//addItem
app.post('/items',(req,res)=>{
	console.log(req.body);
	console.log(loggedUser);

	if(loggedUser.isAdmin === true){
	let newItems = {

			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			isActive: req.body.isActive
		};
		items.push(newItems);
		console.log(items);

		res.send('You have added a new item.');
	} else {

		res.status(403).send('Unauthorized: Action Forbidden.');
	}
})

	
//getAllItem
app.post('/items',(req,res)=>{
	console.log(loggedUser);

	if(loggedUser.isAdmin === true){

		res.send(items);
	} else {
	res.send("Unauthorized: Action Forbidden");
	}
})

//getSingleUser
		//GET requests should not have a request body. It may have headers for additional information or we can add or a small amount of data somewhere else: the url.
		//Route params are values we can pass via the URL
		//This is done especially to allow us to send small amount of data into our server.
		//Route parameters can be defined in the endpoint of a route with :parameterName

		app.get('/users/:index',(req,res)=>{
			//erq.params is an objects that contains the route params.
			//Its properties are then determined by your route parameters
			console.log(req.params);
			//how do we access the actual route params?
			console.log(req.params.index)
			/*console.log(typeof index);*/
			let user = user[index];
			res.send(user);
		})

		//updateUser
			//We're going to update the password of our user. However we should get the user first. To do this we should not pass the index of the user in the body but instead in our route params.

			app.put('/users/:index',(req,res)=>{

				console.log(req.params);
				console.log(req.params.index);
				if(loggedUser !== undefined){
				let userIndex = parseInt(req.params.index);
				//get the proper user from the array with our index:
				//req.body.password comes from the body of your request
				users[userIndex].password = req.body.password;
				console.log(users[userIndex]);
				res.send('User password has been updated')

			} else {

					res.send('Unauthorized. Login first')
				}
			})

			app.get('/items/:index',(req,res)=>{
				console.log.(req.params);
				console.log.(req.params.index);
				let index = parseInt(req.params.index)
				let item = items[index];
				res.send(item);
			})

			app.put('/items/archieve/:index',(req,res)=>{

				console.log(req.params);
				console.log(req.params.index);
				let itemIndex = parseInt(req.params.index);
				if(loggedUser.isAdmin ===true){
					items[itemIndex].isActive = false;
					console.log(items[itemsIndex]);
					res.send('Items archieved')
				} else {

					res.send('Unauthorized: Action Forbidden')
				}
			})

			app.put('/items/activate/:index',(req,res)=>{

				console.log(req.params);
				console.log(req.params.index);
				let itemIndex = parseInt(req.params.index);
				if(loggedUser.isAdmin ===true){
					items[itemIndex].isActive = true;
					console.log(items[itemsIndex]);
					res.send('Item Activated')
				} else {
					res.send('Unauthorized: Activated Forbidden')
				}

			})




app.listen(port, ()=>console.log('Server is running at port ${port}'));

